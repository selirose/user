const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const {
    user
} = require('../../models');
const bcrypt = require('bcrypt');
const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;

passport.use(
    'signup',
    new localStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        async (req, email, password, done) => {
            const hash = bcrypt.hashSync(password, 10);

            user.create({
                email: email,
                password: hash,
                nama: req.body.nama,
            }).then(user => {
                return done(null, user, {
                    message: 'Signup success!'
                });
            }).catch(err => {
                if (err) {
                    if (err.code === 11000 && err.keyValue.nama) {
                        return done(null, false, {
                            message: 'Name already used!'
                        })
                    } else if (err.code === 11000 && err.keyValue.email) {
                        return done(null, false, {
                            message: 'Email already used!'
                        })
                    }
                    return done(null, false, {
                        message: 'User failed to created!'
                    })
                }
            })
        },
    )
)


passport.use(
    'login',
    new localStrategy({
            usernameField: 'email',
            passwordField: 'password'
        },
        async (email, password, done) => {

            const userLogin = await user.findOne({
                email: email

            })

            if (!userLogin) {
                return done(null, false, {
                    message: 'User not found!'
                })
            }

            const validate = await bcrypt.compare(password, userLogin.password);

            if (!validate) {
                return done(null, false, {
                    message: 'Wrong password!'
                })
            }

            return done(null, userLogin, {
                message: 'Login success!'
            })
        }
    )
)
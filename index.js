const express = require('express')
const app = express()
const bodyParser = require('body-parser')

const userRoutes = require('./routes/userRoutes')


app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))

app.use(express.static('public'))

app.use('/', userRoutes);

app.listen(3000,()=> console.log("server running on http://localhost:3000"));
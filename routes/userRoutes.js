const express = require('express');
const router = express.Router();
const passport = require('passport');
const auth = require('../middlewares/auth');
const UserController = require('../controllers/userController');
const userValidator = require('../middlewares/validators/userValidator');


router.get('/', UserController.getOne);

router.post('/signup', [userValidator.signup, function (req, res, next) {
  passport.authenticate('signup', {
    session: false
  }, function (err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.status(401).json({
        status: 'Eror',
        message: info.message
      });
      return;
    }
    UserController.signup(user, req, res, next);
  })(req, res, next);
}]);

router.post('/login', [userValidator.login, function (req, res, next) {
  passport.authenticate('login', {
    session: false
  }, function (err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.status(401).json({
        status: 'Eror',
        message: info.message
      });
      return;
    }
    UserController.login(user, req, res, next);
  })(req, res, next);
}]);


module.exports = router;
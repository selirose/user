const mongoose = require('mongoose')
const mongoose_delete = require('mongoose-delete')

const ReviewSchema = new mongoose.Schema({
  Nama:{
    type:String,
    required:true
  },
  Movie:{
    type:String,
    required:true
  },
  Review:{
    type:String,
    required:true
  },
  Rating:{
    type:Number,
    required:true
  }
}, {
  timestamps:{
    createdAt:'created_at',
    updatedAt:'updated_at'
  },
  versionKey:false
})

ReviewSchema.plugin(mongoose_delete,{overrideMethods:'all'});

module.exports = review = mongoose.model('review', ReviewSchema , 'review');
const mongoose = require('mongoose')

const uri = 'mongodb://localhost:27017/Mini_Project_dev'

mongoose.connect(uri,{useUnifiedTopology:true,useNewUrlParser:true,useCreateIndex: true});

const movie = require('./movie.js')
const user = require('./user.js')
const review = require('./review.js')

module.exports = {movie,user,review}
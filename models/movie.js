const mongoose = require('mongoose')
const mongoose_delete = require('mongoose-delete')

const MovieSchema = new mongoose.Schema({
  Title:{
    type:String,
    required:true
  },
  Year:{
    type:Number,
    required:true
  },
  Released:{
    type:Date,
    required:true
  },
  Runtime:{
    type:String,
    required:true
  },
  Genre:{
    type:Array,
    required:true
  },
  Director:{
    type:String,
    required:false
  },
  Writer:{
    type:String,
    required:false
  },
  Actors:{
    type:Array,
    required:true
  },
  Plot:{
    type:String,
    required:true
  },
  Language:{
    type:String,
    required:true
  },
  Country:{
    type:String,
    required:true
  },
  Poster:{
    type:String,
    required:true
  },
  Ratings:{
    type:mongoose.Schema.Types.Mixed,
    required:true
  },
  Trailer:{
    type:String,
    required:true
  }
}, {
  timestamps:{
    createdAt:'created_at',
    updatedAt:'updated_at'
  },
  versionKey:false
})

MovieSchema.plugin(mongoose_delete,{overrideMethods:'all'});

module.exports = movie = mongoose.model('movie', MovieSchema , 'movie');
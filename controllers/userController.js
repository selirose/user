const {
  user
} = require('../models')
const passport = require('passport');
const jwt = require('jsonwebtoken');

class UserController {

  async signup(err, req, res) {

    const body = {
      email: req.body.email
    };

    const token = jwt.sign({
      user: body
    }, 'secret_password')


    res.status(200).json({
      message: 'Signup success!',
      token: token
    })
  }


  async login(err, req, res) {

    const body = {
      email: req.body.email
    };


    const token = jwt.sign({
      user: body
    }, 'secret_password')

    res.status(200).json({
      message: 'Login success!',
      token: token
    })
  }


  async getOne(req, res) {
    user.findOne({
      email: req.params.email
    }).then(result => {
      res.json({
        status: "Succes get data",
        data: result
      })
    })
  }


}

module.exports = new UserController;
const mongoose = require('mongoose')
const mongoose_delete = require('mongoose-delete')

const UserSchema = new mongoose.Schema({
  nama: {
    type: String,
    index: {
      unique: true
    },
    required: true
  },
  email: {
    type: String,
    index: {
      unique: true
    },
    required: true
  },
  password: {
    type: String,
    required: true,


  },
  // ProfilePicture:{
  //   type:String,
  //   required:false
  // }
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  versionKey: false
})

UserSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
});

module.exports = user = mongoose.model('user', UserSchema, 'user');